

const Course = require("../models/Course");

// Create a new course

// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	})
// 	return newCourse.save().then((course, error) =>{
// 		// Course creaton failed
// 		if(error) {
// 			return false;
// 			// Course creation successful
// 		} else {
// 			return true;
// 		}
// 	})
// };



module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve('User must be ADMIN to access this!');
	return message.then((value) => {
		return {value};
	});
};


// Retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Retrieve Active courses

module.exports.getAllActive = () => {
	return Course.find({isActive:true}).then(result => {
		return result;
	});
};


// Retrieve specific course
module.exports.getCourse = (reqParams)=> {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};


// Update a course

module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
 		description: reqBody.description,
 		price: reqBody.price
	};

	// findByIdAndUpdate(document ID, updatesTobeApplied)
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
};


// ACTIVITY

// PATCH

module.exports.patchCourse = (reqParams) =>{
	let updateActiveField = {
		isActive: false
		

		
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if(error) {
			return false;
		} else {
			return true
		}
	})
};