
const User = require("../models/User");


// "bcrypt" is a password-hashing function that is commonly used in computer systems to a store user password securely
const bcrypt = require("bcrypt");

const auth = require("../auth");

// Check if email already exist
/*
Business Logic:
Use mongoose "find" method to find duplicate emails
Use the "then" method to send a response back to the Postman application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true;
		} else {
			return false;
		};
	});

};

// User Registration
/*
BUSINESS LOGIC
Create a new User object using the mongoose model and the information from the request body
Make sure that the password is encrypted
Save the new User to the database
*/

module.exports.registerUser = (reqBody) => {
	let newUser = new User ({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		// "bcrypt.hashSync" is a function in the bcrypt library that used to generate hash valye for a given input string synchronously
		// "reqBody.password" - input string that needs to be hashed
		// 10 - is value provided as the number of "salt" rounds that the bcrypt algorithm will run in order to encrypt password
		password: bcrypt.hashSync(reqBody.password, 10)
	});

	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return	true;
		}
	})
};


// User Authentication
/*
Business Logic:
1. Check the database if the user email exists
2. Compare the password provided in the login form with the password stored in the database
3. Generate/return a JSON web token if the user is successfully logged in and return false if not
*/

module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result => {
		// user doest not exist
		if(result == null){
			return false
			// user exists
		} else {
			isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
				// if password do not match
			} else {
				return false;
			}
		}
	})
};

// S38 Activity
// Retrieve user details
/*
Business Logic:
1. Find the document in the database using the user's ID
2. Reassign the password of the returned document to an empty string
3. Return the result back to the frontend/postman
*/

module.exports.getProfile = (data) => {
	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
}