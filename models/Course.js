

// ACTIVITY
const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({

	name :{
		type: String,
		required: [true, "name is required"]
	},

	description :{
		type: String,
		required: [true, "description is required"]
	},

	price :{
		type: Number,
		required: [true, "price is required"]
	},

	isActive :{
		type: Boolean,
		default : true
	},

	/*isAdmin :{
		type: Boolean,
		default: false
	},*/

	createdOn :{
		type: Date,
		default: new Date()
	},

	enrollees: [
	{

		userID:{
			type: String,
			required: [true, "userID is required"]
		},

		enrolledOn: {
			type: Date,
			default: new Date()

		}
	}
	]


});

module.exports = mongoose.model("Course", courseSchema);
